#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Xorg Core
sudo pacman -S --noconfirm --needed xorg-server xorg-apps
sudo pacman -S --noconfirm --needed xorg-xinit xorg-twm 
sudo pacman -S --noconfirm --needed xorg-xclock xterm

# Uncomment the below line for VirtualBox
sudo pacman -S  --noconfirm --needed mesa virtualbox-guest-utils

# Network
sudo pacman -S --noconfirm --needed networkmanager
sudo pacman -S --noconfirm --needed network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install lightdm and lxde
sudo pacman -S --noconfirm --needed lxde openbox obconf compton
sudo pacman -S --noconfirm --needed lxmenu-data

# Enable Display Manager
sudo systemctl enable lxdm.service

# Software from 'normal' repositories
sudo pacman -S --noconfirm --needed noto-fonts noto-fonts-emoji tilix
sudo pacman -S --noconfirm --needed adobe-source-code-pro-fonts neofetch
sudo pacman -S --noconfirm --needed chromium geany geany-plugins 
sudo pacman -S --noconfirm --needed conky conky-manager file-roller evince
sudo pacman -S --noconfirm --needed gparted gnome-disk-utility 

